# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's pose(segment) calibration
#
# http://chordata.cc
# contact@chordata.cc
#

# Copyright 2022-2023 Marcos Uriel Maillot, Bruno Laurencich
#
# This file is part of Chordata Motion's pose(segment) calibration.
#
# Chordata Motion's pose(segment) calibration is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's pose(segment) calibration.  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import numpy as np
import pandas as pd
from scipy import signal
import mathutils
import logging
from os import path
from warnings import warn

this_dir = path.dirname(__file__)
def_log_file = path.realpath(path.join(this_dir,'calibration_result.log'))

class Output:
    instance = None
    
    @classmethod 
    def get_instance(cls, do_log = True, log_file = None, reset=False):
        if reset:
            cls.instance = None
        
        if not cls.instance:
            cls.instance = cls(do_log, log_file)
            
        return cls.instance
    
    def __init__(self, do_log = True, log_file = None):
        self.do_log = do_log
        self.logger = logging.getLogger('pose-calibration')
        self.logger.setLevel(logging.DEBUG)
        self.logger.handlers.clear()
        
        if do_log:
            if not log_file: log_file = def_log_file
            fh = logging.FileHandler(log_file, mode='w')
            fh.setLevel(logging.DEBUG)
            self.logger.addHandler(fh)
        
            ch = logging.StreamHandler()
            ch.setLevel(logging.INFO)
            self.logger.addHandler(ch)

    def log(self, *args, **kwargs):
        if not self.do_log: return
        
        msg = " ".join([str(val) for val in args])
        if 'banner' in kwargs.keys() and kwargs['banner']:
            self.logger.info("\n" + "-" * min(len(msg)+4, 80))
            self.logger.info(" " + msg.upper())
            self.logger.info("-" * min(len(msg)+4, 80) + "\n")
        
        elif 'title' in kwargs.keys() and kwargs['title']:
            self.logger.info("\n" + msg)
            self.logger.info("=" * len(msg))

        else:
            self.logger.info(msg)
                
    def debug(self, *args):
        msg = " ".join([str(val) for val in args])
        self.logger.debug(msg)
                      

# function to filter 1 node, all variables
# gyro, acc and mag

def node_filter(node, fc=5, order=3, dt=None):
    df = node.copy()
    # geting sampling freq
    if not dt or dt < 0:
        [n_samples, n_col] = node.shape
        time = node['time(msec)'].to_numpy()
        dt_tot = time[1:n_samples] - time[0:n_samples - 1]
        dt = np.mean(dt_tot)
    fs = 1 / dt * 1000
    # filter creation (low pass butterworth)
    b, a = signal.butter(order, 2 * np.pi * fc, 'lowpass', analog=True)
    z, po = signal.bilinear(b, a, fs)
    # filtering signal:
    p = node.loc[:,['g_x', 'g_y', 'g_z','a_x', 'a_y', 'a_z','m_x', 'm_y', 'm_z']]
    pf = signal.filtfilt(z, po, p, axis=0)
    ref_index = node.index
    df.loc[node.index,['g_x', 'g_y', 'g_z','a_x', 'a_y', 'a_z','m_x', 'm_y', 'm_z']]=pf.astype('int64')
    return df

def apply_rot_2mat(mat, rot):
    for i in range(mat.shape[0]):
        mat_i = mathutils.Vector((mat[i,:]))
        mat_i.rotate(rot)
        mat[i,:] = [mat_i.x, mat_i.y, mat_i.z]
    return mat

def apply_quat_2node(node, quat):
    # obtaining acc, giro and mag data from node
    mat_acc = np.array([node.a_x, node.a_y, node.a_z])
    mat_gyro = np.array([node.g_x, node.g_y, node.g_z])
    mat_mag = np.array([node.m_x, node.m_y, node.m_z])
    # generating mathutils quats
    rot = mathutils.Quaternion(quat)
    # applying vertical rotation to sensor data
    mat_acc = apply_rot_2mat(mat_acc.T, rot)
    mat_gyro = apply_rot_2mat(mat_gyro.T, rot)
    mat_mag = apply_rot_2mat(mat_mag.T, rot)
    # rewrite node with new data
    node.loc[:, ['a_x', 'a_y', 'a_z']] = mat_acc
    node.loc[:, ['g_x', 'g_y', 'g_z']] = mat_gyro
    node.loc[:, ['m_x', 'm_y', 'm_z']] = mat_mag
    return node

def apply_quatmat_2mat(mat, quatmat, timestamps=None):
    assert mat.shape[0] == quatmat.shape[0], "the lengths of the vec and rotation matrix should be equal. mat:{}, quat:{}".format(mat.shape[0],quatmat.shape[0])
    assert mat.shape[1] == 3, "the first matrix should be Nx3, representing a list of vectors"
    assert quatmat.shape[1] == 4, "the second matrix should be Nx4, representing a list of quaterninons "
    
    for i in range(mat.shape[0]):
        mat_i = mathutils.Vector((mat[i,:]))
        vec = mat_i.copy()
        quat_i = mathutils.Quaternion((quatmat[i,:]))
        mat_i.rotate(quat_i)
        mat[i,:] = [mat_i.x, mat_i.y, mat_i.z]
        
    # if timestamps:
        # print("at time ({}): \nvec: {}\n quat:{}\n result:{}".format(timestamps[i], vec, quat_i, mat_i))
    
    return mat

def get_JCS_orientations(node_list):
    trunk_rot = mathutils.Euler((-np.pi/2+np.pi, 0,0)).to_quaternion()
    #limbs_rot = mathutils.Euler((-np.pi/2, 0, 0), 'ZYX') #<--- euler.order = 'nnn' NOT DEFINED IN MATHUTILS-PYTHON
    # see https://gitlab.com/ideasman42/blender-mathutils/-/issues/10
    # this is solved by using 3 single-axis quaternion and combined accordingly
    # single-axis quaternions
    limbs_rot_x = mathutils.Quaternion((1,0,0),-np.pi/2)
    limbs_rot_y = mathutils.Quaternion((1,0,0),0)
    limbs_rot_z = mathutils.Quaternion((1,0,0),0)
    # quaternion composition
    limbs_rot = limbs_rot_z @ limbs_rot_y @ limbs_rot_x

    # new proposal for limb_rot
    limbs_rot = mathutils.Quaternion((1,0,0),-np.pi/2)


    # new foot_rot
    foot_rot = mathutils.Euler((-np.pi/2,-np.pi/2,0)).to_quaternion()

    # added clavicles nodes
    # left clavicle is just ok, in vert_calib we put z positive
    # up (to the sky) and in func_calib the y to the left
    # no correction needed for l-clavicle
    # 180deg correction needed for r-clavicle
    r_clavicle_rot = mathutils.Quaternion((1,0,0),np.pi)
    l_clavicle_rot = mathutils.Quaternion((1,0,0),0)


    out = {}
    for node_name in node_list:
        if node_name in ("base", "dorsal", "neck"):
            out[node_name] = trunk_rot
        # added clavicles nodes
        elif node_name in ("r-clavicle"):
            out[node_name] = r_clavicle_rot
        elif node_name in ("l-clavicle"):
            out[node_name] = l_clavicle_rot

        elif node_name not in ("l-foot", "r-foot"):
            out[node_name] = limbs_rot
        elif node_name in ("l-foot", "r-foot"):
            out[node_name] = foot_rot
            
    return out

def get_output_df(input_df):
    warn("get_output_df is deprecated", stacklevel=2)

    # taking list of nodes to calibrate
    node_list = input_df['node_label'].unique()

    # prepare df to save output (calibrations quaternions)
    df_cali_empty = pd.DataFrame(index = node_list, columns =['w', 'x', 'y', 'z'])
    df_cali_empty.index.name = 'node_label'
    
    return df_cali_empty, node_list.tolist()
