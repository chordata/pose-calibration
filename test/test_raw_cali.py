import pytest

def test_raw(capture_data, calibrator_instance, compare_raw):
    capture, filtered_capture, indexes, node_list = capture_data
    node_list = list(node_list)
    node_list.sort(key=lambda n: 'r-upperarm' not in n)
    calibrator_instance.vert_calib()
    calibrator_instance.func_calib()
    calibrator_instance.heading_calib()
  
    # compare_raw.PRINT_DEBUG=True
    for label in node_list:
        node = filtered_capture[filtered_capture['node_label'] == label]
        # vert_rot_new = calibrator_instance.results['vert_rot'][label]
        vert_rot = compare_raw.vert_calib(node, label, indexes['vert_i'], indexes['vert_s'], False)
        func_i, func_s = calibrator_instance._get_func_range(label)
        func_rot, gyro_int = compare_raw.func_calib(node, label, vert_rot, func_i, func_s, False)
        # func_res = compare_raw.partials['final_func_rot'][label]
        print("GYRO_INT OLD CAL for ", label, compare_raw.partials['gyro_int'][label])
        print("GYRO_INT OLD CAL FINAL for ", label, compare_raw.partials['final_gyro_int'][label])
        print("GYRO_INT NEW CAL for ", label, calibrator_instance.results['gyro_int'][label])
        # gyro_int = compare_raw.partials['gyro_int'][label]
        compare_raw.quat_calib(node, label, vert_rot, func_rot, gyro_int, indexes['vert_i'], indexes['vert_s'], False)
        print("HEADING ROTATION OLD for ", label, compare_raw.partials['heading_rot'][label])
        print("HEADING ROTATION NEW for ", label, calibrator_instance.results['heading_rot'][label])
