import pytest

def test_instance(Calibrator, capture_data, defaults):
    assert Calibrator
    capture, filtered_capture, indexes, _ = capture_data
    
    c = Calibrator(capture, defaults.g_s, defaults.a_s, defaults.m_s, indexes)
